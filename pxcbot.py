#!/usr/bin/env python3
import argparse, json, random, sys, time, urllib.request, websocket

def main(argv):
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'
    api_url = 'https://api.pixelcanvas.io/api'
    baseurl = 'https://europe-west1-pixelcanvasv2.cloudfunctions.net'
    ws_server = 'ws.pixelcanvas.io:8443'

    parser = argparse.ArgumentParser(description='A bot for Pixelcanvas.io')
    parser.add_argument('img_file', help='the image file in raw format')
    parser.add_argument('-f', '--fingerprint', required=True, help='overrides the default user agent')
    parser.add_argument('-u', '--user_agent', default=user_agent, help='the user agent to send to the server')
    parser.add_argument('-w', '--width', required=True, type=int, help='the width of the image')
    parser.add_argument('-x', required=True, type=int, help='X coordinate of the top left corner')
    parser.add_argument('-y', required=True, type=int, help='Y coordinate of the top left corner')
    parser.add_argument('-a', '--alpha', action='store_true', help='provide this argument if the image contains alpha channel')
    parser.add_argument('-o', '--order', help='pixel changing order. Values: normal (default), reverse, random')
    args = parser.parse_args()

    print('Loading image...', end='', flush=True)
    img = read_img(args.img_file, args.alpha)
    print(' Done')

    height = len(img) // args.width

    endx = args.x + args.width
    endy = args.y + height

    chunkidx = calc_chunk(args.x, args.y, endx, endy)
    offset = get_offset(args.x, args.y)

    print('Gathering pixels to be changed...', end='', flush=True)
    ptc = load_chunks(*chunkidx, img, args.x, args.y, args.width, height, *offset, api_url, args.user_agent)
    print(' Done')

    ws = websocket.create_connection('wss://' + ws_server + '/?fingerprint=' + args.fingerprint)
    print('Receiving updates via websocket')
    timeout = time.time()
    while True:
        if timeout < time.time():
            pixel = choose_pixel(ptc, args.order)
            timeout = place_pixel(pixel, baseurl, args.fingerprint, args.user_agent)
        else:
            try:
                result = ws.recv()

                ux = ((result[6] >> 4) + ((result[5] & 3) << 4) + (result[2] << 6) + (result[1] << 14) << 2)
                bx = ux.to_bytes(3, byteorder='big', signed=False)
                x = int.from_bytes(bx, byteorder='big', signed=True) >> 2

                uy = ((result[5] >> 2) + (result[4] << 6) + (result[3] << 14)) << 2
                by = uy.to_bytes(3, byteorder='big', signed=False)
                y = int.from_bytes(by, byteorder='big', signed=True) >> 2

                c = result[6] & 15

                if x >= args.x and x < args.x + args.width and y >= args.y and y < args.y + height:
                    idx = ptc_get(ptc, x, y)
                    if idx is None:
                        color = img[x - args.x + (y - args.y) * args.width]
                        if color is not None:
                            ptc.append([x, y, color])
                    else:
                        if ptc[idx][2] == c:
                            del ptc[idx]

                    print_process(x, y, c, len(img) - len(ptc), len(img))
            except websocket._exceptions.WebSocketConnectionClosedException:
                print('Websocket connection has been closed. Reconnecting...')
                time.sleep(5)
                result = ws.recv()
            except:
                ws.close()
                raise

def load_chunks(start_chunk_x, start_chunk_y, chunk_count_x, chunk_count_y,
new_img, start_x, start_y, width, height, offset_x, offset_y, baseurl, user_agent):
    ptc = []
    for y2 in range(chunk_count_y):
        for x2 in range(chunk_count_x):
            req = urllib.request.Request(
            baseurl + '/bigchunk/' + str((start_chunk_x + x2) * 15) + '.' + str((start_chunk_y + y2) * 15) + '.bmp',
            data = None,
            headers = {
                'User-Agent': user_agent
            })
            f = urllib.request.urlopen(req)

            for y1 in range (15):
                for x1 in range (15):
                    for y0 in range (64):
                        y = y0 + y1 * 64 + y2 * 960
                        for x0 in range (0, 64, 2):
                            byte = int.from_bytes(f.read(1), byteorder = 'big')
                            x = x0 + x1 * 64 + x2 * 960
                            add_ptc(ptc, new_img, x - offset_x, y - offset_y, width, height, start_x, start_y, byte >> 4)
                            add_ptc(ptc, new_img, x + 1 - offset_x, y - offset_y, width, height, start_x, start_y, byte % 16)
    return ptc

def calc_chunk(x, y, end_x, end_y):
    start_chunk_x = (x + 448) // 960
    start_chunk_y = (y + 448) // 960
    chunk_count_x = (end_x + 448) // 960 - start_chunk_x + 1
    chunk_count_y = (end_y + 448) // 960 - start_chunk_y + 1
    return start_chunk_x, start_chunk_y, chunk_count_x, chunk_count_y

def get_offset(start_x, start_y):
    offset_x = (start_x + 448) % 960
    offset_y = (start_y + 448) % 960
    return offset_x, offset_y

def place_pixel(pixel, baseurl, fingerprint, user_agent):
    req = urllib.request.Request(
        baseurl + '/pixel',
        data = json.dumps({
            'color': pixel[2],
            'fingerprint': fingerprint,
            'token': None,
            'wasabi': pixel[0] + pixel[1] + 2342,
            'x': pixel[0],
            'y': pixel[1]
        }).encode('utf-8'),
        headers = {
            'User-Agent': user_agent,
            'Content-Type': 'application/json',
            'Origin': 'https://pixelcanvas.io'
        })
    try:
        f = urllib.request.urlopen(req)
        resp = json.loads(f.read())
        print('Placed pixel', pixel, 'response:', resp)
        return time.time() + resp['waitSeconds'] - 2
    except urllib.error.HTTPError as e:
        print('HTTP error', e.code, e.reason)
        
        # This code below is not working anymore due to the changes in the pixelcanvas code
        # A workaround is needed
        
        #resp = json.loads(e.read())
        #if resp['errors'][0]['msg'] == 'You must provide a token':
        #    print('We must provide a token.');
        #    sys.exit(1)
        #elif resp['errors'][0]['msg'] == 'You must wait':
        #    print('We must wait for ' + str(resp['waitSeconds']) + 's.')
        #    return time.time() + resp['waitSeconds'] - 2
        #else:
        #    print('Unknown error.')
        #    print(resp)
        #    sys.exit(1)
        
        sys.exit(1)
    except urllib.error.URLError as e:
        print('URL error')
        print('reason', e.reason)
        print(e.read())
        sys.exit(1)

def add_ptc(ptc, new_img, x, y, width, height, start_x, start_y, old_color):
    if x >= 0 and y >= 0 and x < width and y < height:
        new_color = new_img[x + y * width]
        if new_color is not None and new_color != old_color:
            ptc.append([start_x + x, start_y + y, new_color])

def ptc_get(ptc, x, y):
    for i, pixel in enumerate(ptc):
        if pixel[0] == x and pixel[1] == y:
            return i

def read_img(file, alpha):
    img = []
    with open(file, 'rb') as f:
        color = f.read(1)
        if alpha:
            while color:
                alpha = f.read(1)
                if int.from_bytes(alpha, byteorder='little') > 0:
                    img.append(int.from_bytes(color, byteorder='little'))
                else:
                    img.append(None)
                color = f.read(1)
        else:
            while color:
                img.append(int.from_bytes(color, byteorder='little'))
                color = f.read(1)
    return img

def choose_pixel(ptc, order):
    if order == 'reverse':
        return ptc[len(ptc) - 1]
    if order == 'random':
        return random.choice(ptc)
    return ptc[0]

def print_process(x, y, color, remaining_size, image_size):
    process_per_cent = remaining_size / image_size * 100
    print("Changed pixel [%d, %d, %d] | %d%% (%d/%s)" % (x, y, color, process_per_cent, remaining_size, image_size))

if __name__ == "__main__":
    main(sys.argv[1:])
