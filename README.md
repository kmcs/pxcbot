# pxcbot

A bot for Pixelcanvas.io

## Usage

`pxcbot.py -f FINGERPRINT [-u USER_AGENT] -w WIDTH -x X -y Y [-a] img_file`

* `FINGERPRINT` is the fingerprint to send to the Pixelcanvas server (place a pixel while the developer tools of your browser is open to see it)
* `USER_AGENT` is to override the default user agent (Firefox 61) to send
* `WIDTH` is the width of the image
* `X` is the X coordinate of the top left corner on Pixelcanvas
* `Y` is the Y coordinate of the top left corner on Pixelcanvas
* `img_file` is the image file in raw format
* Provide `-a` if the image has an alpha channel

## Converting your image using GIMP

1. Go to Windows --> Dockable Dialogs --> Palettes
2. Right-click and choose Import Palettes
3. Choose palette file and import the included palette
4. Open your image
5. (optional) Edit/resize your image
6. Convert your image to indexed using Image --> Mode --> Indexed
7. Select Use custom palette and the imported palette
8. Uncheck Remove unused and duplicate colors from colormap
9. Select Floyd-Steinberg normal or reduced color bleeding color dithering
10. Click Convert
11. Export image
12. As the file type, select raw image data and click Export
13. Leave the default settings and click Export
